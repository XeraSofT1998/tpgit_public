# coding: utf-8
"""
GUI PhoneBook
Created on Tue feb 16 19:05:00 2021

@author: DUPLAN Alexandre

"""
from tkinter import Entry, Label, StringVar, Tk, Button  #use Tkinter to create GUI
from individu import Individu  #import the class individu

#initialise the dico to storage the individu
DICO_INDIVIDU = {}

def print_dict():
    """print the dico containing the contacts"""
    print(DICO_INDIVIDU)

def insert_contact():
    """ Function to insert new individu """
    #if the field "name" in the the window isn't empty
    if WIDGETS_ENTRY["name"].get() is not None:

        #if the name doesn't exist in the dictionary
        if WIDGETS_ENTRY["name"].get() not in DICO_INDIVIDU:

            #create the individu with entry from the GUI tkinter
            someone = Individu(WIDGETS_ENTRY["name"].get(),
                               WIDGETS_ENTRY["last_name"].get(),
                               WIDGETS_ENTRY["phone"].get(),
                               WIDGETS_ENTRY["adress"].get(),
                               WIDGETS_ENTRY["city"].get())
            #save to annuaire.txt
            someone.save()
            print(someone.name, "has been inserted")
            #save in dictionary individu
            DICO_INDIVIDU[WIDGETS_ENTRY["name"].get()] = someone
        else:
            print(WIDGETS_ENTRY["name"].get(), "already exist")
    else:
        print("field is empty")

def load_contact():
    """function to load the actual contacts from annuaire.txt to dictionary """
    try:
        with open("annuaire.txt", 'r') as outfile:
            old_annuaire = outfile.readlines()
            for line in old_annuaire:
                temp = line.replace(":", ",")
                temp = temp.replace(" ", "")
                temp2 = temp.split(",")

                #create individu from data in annuaire.txt
                someone = Individu(temp2[1], temp2[3], temp2[5], temp2[7], temp2[9])
                #and put them in the individu dictionary
                DICO_INDIVIDU[temp2[1]] = someone
    except:
        file = open("annuaire.txt", 'w')
load_contact()

def finder_contact():
    """function to get the name from the field in GUI tkinter
       and complte the other field with the individu details.
    """
    name_proposed = WIDGETS_ENTRY["name"].get()
    for key, value in DICO_INDIVIDU.items():
        if name_proposed == key:
            var1 = StringVar()
            var1.set(value.last_name)
            WIDGETS_ENTRY["last_name"].config(text=var1)

            var2 = StringVar()
            var2.set(value.phone)
            WIDGETS_ENTRY["phone"].config(text=var2)

            var3 = StringVar()
            var3.set(value.adress)
            WIDGETS_ENTRY["adress"].config(text=var3)

            var4 = StringVar()
            var4.set(value.city)
            WIDGETS_ENTRY["city"].config(text=var4)

def delete_function():
    """
    function to delete an individu from entry field to annuaire.text
    """
    name_proposed = WIDGETS_ENTRY["name"].get() + ","
    name_key = WIDGETS_ENTRY["name"].get()

    #get the new annuaire list
    with open("annuaire.txt", 'r') as outfile:
        old_annuaire = outfile.readlines()
        compt_line = 0
        for line in old_annuaire:
            if name_proposed in line:
                del old_annuaire[compt_line]
                break
            compt_line += 1

    #input the new annuaire list in annuaire.txt
    with open("annuaire.txt", 'w') as outfile:
        for line in old_annuaire:
            outfile.write(line)

    #delete the individu from the dictionary
    for key in DICO_INDIVIDU:
        if key == name_key:
            DICO_INDIVIDU.pop(key)

#create a window
ROOT = Tk()
ROOT.title('Annuaire')
ROOT.geometry('420x150')

#list to create the widgets
IDS = ["name", "last_name", "phone", "adress", "city"]
BOUTON = ["Research", "Insert", "Delete", "Print_dico"]

#dictionary to save created widgets
WIDGETS_LABS = {}
WIDGETS_ENTRY = {}
WIDGETS_BUTTON = {}

i, j = 0, 0

#loops to create Labels, entries and buttuns
for idi in IDS:
    lab = Label(ROOT, text=idi)
    WIDGETS_LABS[idi] = lab
    lab.grid(row=i, column=0)

    var = StringVar()
    entry = Entry(ROOT, text=var)
    WIDGETS_ENTRY[idi] = entry
    entry.grid(row=i, column=1)

    i += 1

for idi in BOUTON:
    button = Button(ROOT, text=idi)
    WIDGETS_BUTTON[idi] = button
    button.grid(row=i+1, column=j)

    j += 1

#configure command to buttuns
WIDGETS_BUTTON["Insert"].config(command=insert_contact)
WIDGETS_BUTTON["Print_dico"].config(command=print_dict)
WIDGETS_BUTTON["Research"].config(command=finder_contact)
WIDGETS_BUTTON["Delete"].config(command=delete_function)

#mainloop the window tkinter
ROOT.mainloop()
