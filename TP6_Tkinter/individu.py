# coding: utf-8
#!/usr/bin/python
"""
Individu class
Created on Tue feb 02 18:00:54 2021

@author: DUPLAN Alexandre

"""
class Individu:
    """ class constructor """
    def __init__(self, name, last_name, phone, adress, city):
        self.name = name
        self.last_name = last_name
        self.phone = phone
        self.adress = adress
        self.city = city

    def __str__(self):
        return " %s,%s,%s,%s,%s " %(self.name,
                                    self.last_name,
                                    self.phone,
                                    self.adress,
                                    self.city)

    def __repr__(self):
        return " %s, %s, %s, %s, %s " %(self.name,
                                        self.last_name,
                                        self.phone,
                                        self.adress,
                                        self.city)

    def save(self):
        """
        function to save the individu in a file.txt
        """
        info = 'name: {}, last_name: {}, phone: {}, adress: {}, city: {}'.format(self.name,
                                                                                 self.last_name,
                                                                                 self.phone,
                                                                                 self.adress,
                                                                                 self.city)
        with open("annuaire.txt", 'a') as outfile:
            outfile.write(info)
            outfile.write(",  \n")
