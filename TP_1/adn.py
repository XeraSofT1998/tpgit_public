#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Script Python dans le cadre de l'UE INLO de M1 Bioinformatique.
"""
__author__ = 'DUPLAN ALEXANDRE'


def is_valid(adn_str):
   n=0
   for nucleotide in adn_str:
      if nucleotide not in  ('ATCGatcg'):
         n=n+1
      else:
         pass
   if n==0:
      print('TRUE')
      return ('TRUE')
   else :
      print('FALSE')
      return ('FALSE')

def get_valid_adn(seq_input=input("Enter a DNA sequence : ")):
   n=0
   while n != 1:
      if is_valid(seq_input)=='TRUE':
         print('sequence validate')
         n=1
      elif is_valid(seq_input)=='FALSE':
         get_valid_adn(seq_input=input("Enter a DNA sequence : "))
      else:
         print('error')
   return seq_input
