#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 20:30:38 2021
@author: DUPLAN Alexandre
"""

import re

TRANSCRIPT_START = dict()
TRANSCRIPT_END = dict()

def get_tx_genomic_length(input_file=None):
    """
    input GTF file
    return transcripts lenght
    """

    file_handler = open(input_file)
    for line in file_handler:
        token = line.split("\t")
        start = int(token[3])  # start of current element
        end = int(token[4])  # end of current element
        tx_id = re.search('transcript_id "([^"]+)"', token[8]).group(1)  # transcript ID

        if tx_id not in TRANSCRIPT_START:
            TRANSCRIPT_START[tx_id] = start
            TRANSCRIPT_END[tx_id] = end
        else:
            if start < TRANSCRIPT_START[tx_id]:
                TRANSCRIPT_START[tx_id] = start

                if end > TRANSCRIPT_END[tx_id]:
                    TRANSCRIPT_END[tx_id] = end

    for tx_id in TRANSCRIPT_START:
        print(tx_id + "\t" + str(TRANSCRIPT_END[tx_id] - TRANSCRIPT_START[tx_id] + 1))


if __name__ == '__main__':
    get_tx_genomic_length(input_file='../pymetacline/data/gtf/simple.gtf')
