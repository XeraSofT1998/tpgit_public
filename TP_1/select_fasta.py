#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A Cli to select sequence from fasta file
Created on Tue Jan 19 20:30:48 2021
@author: DUPLAN Alexandre
"""

import argparse

def create_parser():
    """
    This function create a parser to add arguments
    """

    parser = argparse.ArgumentParser(add_help=True,
                                     description="""[Select sequences
                                     from a fasta file]""")

    parser.add_argument('-i','--inputfile',
                        type=argparse.FileType('r'),
                        metavar='FASTA',
                        required=True,
                        help="[Where is the fasta file]")

    parser.add_argument('-s','--seq',
                        type=str,
                        required=True,
                        nargs="+",
                        help="[sequences IDs]")
    return parser

def parse_id_to_seq(inputfile, sequence_list):
    """
    file parser to retreive sequence from sequence IDs
    Argument:
        inputfile : fasta file
        sequence_list : list of sequences IDs from the fasta file
    """

    dico_seq = {}
    for seq_id in sequence_list:
        for line in inputfile:
            if line.startswith(">"+seq_id):
                continue
            if line.startswith(">"):
                break
            if seq_id in dico_seq:
                dico_seq[seq_id] += line.replace("\n","")
            else:
                dico_seq[seq_id] = line.replace("\n","")

    for seq_id, seq in dico_seq.items():
        print(seq_id+"\n"+seq+"\n")

def main():
    """
    This function launch the program to return
    specific sequences for IDs from a fasta file.
    """

    parser = create_parser()
    args = parser.parse_args()
    args = args.__dict__
    parse_id_to_seq(args["inputfile"], args["seq"])
    print(args["inputfile"])

if __name__ == "__main__":
    main()
