# coding: utf-8
#!/usr/bin/python
"""
Binary tree
Created on Tue feb 02 18:00:54 2021

@author: DUPLAN Alexandre

"""
from tree_file import TreeNode
from tree_file import if_node_exists
from tree_file import print_tree

def test_tree():
    #Tree structure : Node
    tree = TreeNode("root")
    node_1 = TreeNode("child1_R")
    node_2 = TreeNode("child1_L")
    node_3 = TreeNode("child2_R")
    node_4 = TreeNode("child2_L")
    node_5 = TreeNode("child3_R")
    node_6 = TreeNode("child3_L")
    #Tree structure : Link between node
    tree.right_child = node_1
    tree.left_child = node_2
    node_1.right_child = node_3
    node_1.left_child = node_4
    node_2.right_child = node_5
    node_2.left_child = node_6
    #print the print
    print_tree(tree)
    print("__^___TREE_PRINTED___^__")
    #check if a node exist
    print(if_node_exists(tree, "child3_L"))
    print(if_node_exists(tree, "node_doesnt_exist"))
    #insert a new node
    tree.insert_node("new_node", "left_child", "left_child")
    print("__new_node_tree__")
    print_tree(tree)


if __name__ == "__main__":
    test_tree()
