# coding: utf-8
#!/usr/bin/python
"""
Binary tree
Created on Tue feb 16 19:05:00 2021

@author: DUPLAN Alexandre

"""

class TreeNode():
    """class constructor """
    def __init__(self, value):
        self.left_child = None
        self.right_child = None
        self.root = value

    def is_leaf(self):
        return self.right_child is None and self.left_child is None

    def __str__(self):
        return str(self.root)

    def __repr__(self):
        return "TreeNode(%s, left: %s, right: %s)" % (self.root,
                                                      self.left_child,
                                                      self.right_child)
    def get_left_child(self):
        return self.left_child

    def get_right_child(self):
        return self.right_child

    def set_node_calue(self, value):
        self.root = value

    def get_node_value(self):
        return self.root

    def insert_node(self, new_node, parents_link, child_link):
        """
        insert a new node after a chosen parent node
        Parameters
        ----------
        new_node : name of the new node

        node_parent : name of the node to input the new node

        parents_link : where you want input the new node
        (left_child or right_child)

        child_link : where you want input the previous child of the node
        (left_child or right_child)
        """
        #create the new node
        new_node = TreeNode(str(new_node))

        #if the new node have to be linked to the right at the parents
        if str(parents_link) == "right_child":

            #if the parent's node had already a right child save to input later
            if self.right_child is not None:
                temp = self.right_child

            #link the parent node to the new node
            self.right_child = new_node

        #if the new node have to be linked to the right at the parents
        elif str(parents_link) == "left_child":

            #if the parent's node had already a left child save to input later
            if self.left_child is not None:
                temp = self.left_child

            #link the parent node to the new node
            self.left_child = new_node

        #input the previous child to the new node at the chosen side
        if temp is not None:
            if str(child_link) == "right_child":
                new_node.right_child = temp
            elif str(child_link) == "left_child":
                new_node.left_child = temp


def if_node_exists(tree, value):
    """
    check in the tree if the node exist
    Parameters
    ----------
    tree : tree with nodes
    value :name of the node to check if is existing
    """
    if tree is None:
        return False
    if tree.root == value:
        return True
    # then recur on left subtree
    res1 = if_node_exists(tree.left_child, value)
    # node found, no need to look further
    if res1:
        return True
    #node is not found in left, so recur on right subtree
    res2 = if_node_exists(tree.right_child, value)
    return res2

def print_tree(root, level=0):
    """
    Print the selected node (root) and all his child
    Parameters
    ----------
    root : the first node chosen "(last ancester of the wanted tree)
    """
    if root is not None:
        print_tree(root.get_right_child(), level + 1)
        print(' ' * 4 * level + '->', root.get_node_value())
        print_tree(root.get_left_child(), level + 1)
