# coding: utf-8
"""
Created on Wed Jan 27 21:01:05 2021

@author : Alexandre DUPLAN

train to create "class"
create chained list with node and move on different nodes to insert/delete nodes
"""

from node import Node

class ChainedList:
    """
    Chained list Object
    Parameters
    ----------
    nodes : list
        list that we want to transfert in a chained list of Node object
    """
    def __init__(self, nodes):
        """
        Class constructor
        """

        #initiate the first node
        self.first_node = Node(nodes[0])
        current_node= self.first_node

        #for each element in nodes list
        for element in nodes :
            # link a new node
            current_node.link = Node(element)
            #move on the new node
            current_node = current_node.link

    # function str and repr to avoid the original object representation
    #example : <chainedList.ChainedList object at 0x000001E145ED4670>
    def __str__(self):
        """
        This method returns the object as string.
        """
        return str(self.first_node)

    def __repr__(self):
        """
        This method returns a representation of a class
        """
        return "ChainedList(%s)" % (str(self.first_node))

    def insert_node_after(self, data, new_node):
        """
        insert a new node after the node with the value == data
        Parameters
        ----------
        data : searched data
        new_node : node to insert
        """

        #initalise the position of current node at first node
        current_node = self.first_node

        while current_node is not None:
            #if the current node the one we are looking for to add a new node -> break
            if current_node.data == data:
                break
            #else go on the child node
            else :
                current_node = current_node.link

        #create a new node and link
        new_node = Node(new_node)
        #link the new node to current child node
        new_node.link = current_node.link
        #link the current node to the new node
        current_node.link = new_node

    def delete_node(self, data):
        """
        delete all node(s) value == data
        Parameters
        ----------
        data : searched data to delete
        return -> None
        """
        #check if first node had the input data (to be delete)
        if self.first_node.data == data:
            #if the first node have to be delete, the current child node will be the new root
            self.first_node = self.first_node.link

        #search to find the node to delete
        current_node = self.first_node

        #loop : while the node have child
        while current_node.link is not None:
            #if the child node have the good data (to be delete)
            if current_node.link.data == data:
                #deletion
                current_node.link = current_node.link.link
            current_node = current_node.link
