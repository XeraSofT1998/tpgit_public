# coding: utf-8
"""
Created on Wed Jan 27 21:01:05 2021

@author : Alexandre DUPLAN

create Class Node for chained list
"""

class Node:
    def __init__(self, param_data):
        """
        Class constructor
        """
        self.data = param_data
        self.link = None

    # function str and repr to avoid the original object representation
    #example : <chainedList.ChainedList object at 0x000001E145ED4670>
    def __str__(self):
        liste = []
        node = self
        while node.link != None:
            liste.append(node.data)
            node = node.link

        liste.append(node.data)
        return str(liste)

    def __repr__(self: object) -> str:
        return "%s" % (self.data)
