# coding: utf-8
"""
Created on Wed Jan 27 21:01:05 2021

@author : Alexandre DUPLAN

test the class created in node.py and chainedList.py
"""

from node import Node
from chainedList import ChainedList

def test_print_node():
    """
    This function tests the Node Class
    """
    #nodes
    Root = Node(1)
    node_two = Node(5)
    node_three = Node(6)
    node_four = Node(12)
    node_five = Node(34)
    #linkbetween nodes
    Root.link = node_two
    node_two.link = node_three
    node_three.link = node_four
    node_four.link = node_five
    #chained list : node & link created
    print(Root)


def test_insert_node():
    """
    This function inserts the node in chainedList
    """

    print("chained_list before insert")
    chained_list = ChainedList([1,5,6,12,34])
    print(chained_list)
    chained_list.insert_node_after(12, Node(30)) #let input the node 35 after node 34
    print("chained_list after insert")
    print(chained_list)

def test_delete():
    """
    This function tests the deletion node
    """

    chained_list = ChainedList([1,5,6,12,34,54,90,25])
    print(chained_list)
    chained_list.delete_node(12)
    print(chained_list)
    chained_list.insert_node_after(6, Node(12))

if __name__ == "__main__":
    # just a test to see how the __str__ method of node work's

    test_print_node()
    test_insert_node()
    test_delete()
