# coding: utf-8
#!/usr/bin/python
"""
Created on Mon feb 08 12:00:00 2021

@author: DUPLAN Alexandre
"""
#import Unit testing framework for testing
import unittest
#import the class node and linkedlist to create linked list
from linkedList import Node, LinkedList

class TestChainedList(unittest.TestCase):
    """
    test the LinkedList class
    """

    def setUp(self):
        """
        test initialization
        """
        self.linked_list = LinkedList(["a", "b", "c", "d", "e"])

    def test_created_linkedlist_is_empty(self):
        """
        1. test if a newly created LinkedList is empty
        """
        #create some linkedlists (empty or not)
        empty_list = LinkedList([])
        not_empty_list = LinkedList(["a", "b", "c"])

        #method to test if the created linkedlist is empty
        self.assertIsNotNone(empty_list.head)
        self.assertIsNotNone(not_empty_list.head)

    def test_linkedlist_is_empty_after_add(self):
        """
        2. any linkedlist on which an element has just been linked is not empty
        """
        #create a linkedlist
        list_to_add = LinkedList(["a"])

        #add node at the created linkedlist
        list_to_add.add_after("a", Node("b"))

        #method to test if the linked list is empty after adding a node
        self.assertIsNotNone(list_to_add.head)

    def test_linkedlist_is_unchanged(self):
        """
        3. a linkedlist which undergoes an add_node followed by a remove_node
        is unchanged
        """
        #create a original linkedlist and a copy
        original = LinkedList(["a", "b", "c", "d", "e"])
        copy = str(original)

        #add node and remove it
        original.add_after("e", Node("f"))
        original.remove_node("f")

        #method to test if the linkedlist is unchanged
        self.assertEqual(str(original), copy)

        # second method: i want to raise an Exception
        #if str(original) == copy:
        #    raise Exception("The list is unchanged")

    def test_top_element_after_add(self):
        """
        4. check if the top of a linkedlist to which we just added an element
        x is this element x
        """
        #add element (node) to a linkedlist
        node_to_add = Node("x")
        list_1 = LinkedList(["a", "b", "c", "d", "e"])
        list_1.add_before("a", node_to_add)

        #method to test if the top of the linkedlist is the added node
        self.assertEqual(node_to_add, list_1.head)

        # second method: i REALLY want to raise an Exception (again)
        #if list_1.head == node_to_add:
        #    raise Exception("The top of the linkedlist is the added x element")

if __name__ == '__main__':
    unittest.main()
